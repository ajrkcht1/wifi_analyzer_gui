# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Jun 17 2015)
## http://www.wxformbuilder.org/
## https://sourceforge.net/projects/objectlistview/files/objectlistview-python/v1.2/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

from auiNotebook import AuiNotebook
import wx
import wx.xrc
import wx.aui
import wx.richtext

###########################################################################
## Class MyFrame
###########################################################################


class MyFrame(wx.Frame):
    def __init__(self, parent):
        wx.Frame.__init__(self, parent, id=wx.ID_ANY, title=wx.EmptyString, pos=wx.DefaultPosition,
                          size=wx.Size(986, 717), style=wx.DEFAULT_FRAME_STYLE | wx.TAB_TRAVERSAL)
        # Widgets:
        self.aui_notebook = None
        self.status_bar = None
            # Menu:
        self.menu_bar = None
        self.menu_database = None
        self.menu_help = None
        self.menu_item_exit = None
        self.menu_item_kibana = None
        self.menu_item_about = None

        # Build the windows
        self.menu_file = wx.Menu()
        self.set_window_size()
        self.set_menu_bar()
        self.set_frame()

        self.controller_handler = self.aui_notebook.splitter

    def set_window_size(self):
        """
            Set minimum size and maximum size of windows.
        """
        self.SetSizeHintsSz(wx.Size(850, 625), wx.DefaultSize)

    def set_menu_bar(self):
        self.menu_bar = wx.MenuBar(0)

        self.menu_file = wx.Menu()
        self.menu_database = wx.Menu()
        self.menu_help = wx.Menu()

        self.menu_item_exit = wx.MenuItem(self.menu_file, wx.ID_ANY, "Exit", wx.EmptyString, wx.ITEM_NORMAL)
        self.menu_item_kibana = wx.MenuItem(self.menu_database, wx.ID_ANY, "Open Kibana Database", wx.EmptyString,  wx.ITEM_NORMAL)
        self.menu_item_about = wx.MenuItem(self.menu_help, wx.ID_ANY, "About...", wx.EmptyString, wx.ITEM_NORMAL)

        self.menu_file.AppendItem(self.menu_item_exit)
        self.menu_database.AppendItem(self.menu_item_kibana)
        self.menu_help.AppendItem(self.menu_item_about)

        self.menu_bar.Append(self.menu_file, "File")
        self.menu_bar.Append(self.menu_database, "Database")
        self.menu_bar.Append(self.menu_help, "Help")

        self.SetMenuBar(self.menu_bar)

    def set_frame(self):
        frame_sizer = wx.BoxSizer(wx.VERTICAL)
        self.aui_notebook = AuiNotebook(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.aui.AUI_NB_DEFAULT_STYLE)
        frame_sizer.Add(self.aui_notebook, 1, wx.ALL | wx.EXPAND, 1)

        self.SetSizer(frame_sizer)
        self.Layout()
        self.status_bar = self.CreateStatusBar(1, wx.ST_SIZEGRIP, wx.ID_ANY)

        self.Centre(wx.BOTH)

    def add_page(self, name):
        self.aui_notebook.add_profile(name, self.aui_notebook, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)

app = wx.PySimpleApp()
frame = MyFrame(None)
frame.Show()
app.MainLoop()