import os
import webbrowser
from functools import partial
import wx
import wx.richtext
import ObjectListView as OLV
from ObjectListView import ObjectListView, ColumnDefn


def set_columns(olv, columns):
    """
        <List> columns = [ (attribute_name, column_name, column_width, column_image), ...]
        column_image = -1   # No Image
    """
    olv_columns = list()
    for attribute_name, column_name, column_width, column_image in columns:
        olv_columns.append(ColumnDefn(column_name, "left", column_width, attribute_name, imageGetter=column_image))
    olv.SetColumns(olv_columns)


def write_to_shell(shell, output):
    shell.SetInsertionPoint(len(shell.GetValue()))
    shell.WriteText(output)
    shell.ScrollLines(shell.GetNumberOfLines())


class Splitter(wx.SplitterWindow):
    def __init__(self, *args, **kwargs):
        wx.SplitterWindow.__init__(self, *args, **kwargs)
        # Controller-Variables:
        self.shell = None
        self.computers_options = None
        self.processes_options = None
        self.services_options = None
        # GUI-Variables:
        self.computers_menu = None
        self.processes_menu = None
        self.services_menu = None
        self.top_panel = None
        self.bottom_panel = None

    @staticmethod
    def build_main_splitter(*args, **kwargs):
        splitter = Splitter(*args, **kwargs)
        splitter.Bind(wx.EVT_IDLE, splitter.main_splitter_on_idle)
        splitter.SetMinimumPaneSize(30)

        # Top side:
        splitter.top_panel = wx.Panel(splitter, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
            # Widgets in top side:
        computers_static_sizer = wx.StaticBoxSizer(wx.StaticBox(splitter.top_panel, wx.ID_ANY, "Computers"), wx.VERTICAL)
        splitter.computers_list_ctrl = ObjectListView(computers_static_sizer.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                                      wx.LC_AUTOARRANGE | wx.LC_HRULES | wx.LC_REPORT | wx.LC_SINGLE_SEL | wx.LC_VRULES)
        computers_static_sizer.Add(splitter.computers_list_ctrl, 1, wx.ALL | wx.EXPAND, 1)
            # Set widgets in panel:
        splitter.top_panel.SetSizer(computers_static_sizer)
        splitter.top_panel.Layout()
        computers_static_sizer.Fit(splitter.top_panel)

        # Bottom side:
        splitter.bottom_panel = wx.Panel(splitter, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
            # Widgets in Bottom side:
        shell_static_sizer = wx.StaticBoxSizer(wx.StaticBox(splitter.bottom_panel, wx.ID_ANY, "Shell"), wx.VERTICAL)
        splitter.shell_rich_text = wx.richtext.RichTextCtrl(shell_static_sizer.GetStaticBox(), wx.ID_ANY,
                                                            wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                                            wx.TE_AUTO_URL | wx.TE_PROCESS_ENTER | wx.TE_PROCESS_TAB | wx.TE_READONLY | wx.VSCROLL | wx.HSCROLL | wx.NO_BORDER | wx.WANTS_CHARS)
        splitter.shell_rich_text.SetDefaultStyle(wx.richtext.RichTextAttr(wx.TextAttr(wx.GREEN, wx.BLACK)))
        splitter.shell_rich_text.SetBackgroundColour(wx.BLACK)
        splitter.shell_rich_text.SetForegroundColour(wx.GREEN)
        splitter.shell_rich_text.BeginTextColour('green')
        splitter.write_introduction_to_shell()

        splitter.command_text_ctrl = wx.TextCtrl(shell_static_sizer.GetStaticBox(), wx.ID_ANY,
                                                 wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                                 wx.TE_AUTO_URL | wx.TE_PROCESS_ENTER | wx.TE_PROCESS_TAB | wx.TE_RICH | wx.NO_BORDER)
        splitter.command_text_ctrl.SetDefaultStyle(wx.TextAttr(wx.GREEN, wx.BLACK))
        splitter.command_text_ctrl.SetBackgroundColour(wx.BLACK)
        splitter.command_text_ctrl.SetForegroundColour(wx.GREEN)
        shell_static_sizer.Add(splitter.shell_rich_text, 1, wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT, 1)
        shell_static_sizer.Add(splitter.command_text_ctrl, 0, wx.EXPAND | wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.TOP, 1)
            # Set widgets in panel:
        splitter.bottom_panel.SetSizer(shell_static_sizer)
        splitter.bottom_panel.Layout()
        shell_static_sizer.Fit(splitter.bottom_panel)

        # Set sides in splitter:
        splitter.SplitHorizontally(splitter.top_panel, splitter.bottom_panel, 413)

        # Connect Events:
        splitter.computers_list_ctrl.Bind(wx.wx.EVT_LIST_ITEM_RIGHT_CLICK, splitter.process_item_right_click)
        splitter.command_text_ctrl.Bind(wx.EVT_TEXT_ENTER, splitter.on_text_enter_command_text_ctrl)
        splitter.command_text_ctrl.Bind(wx.EVT_TEXT_URL, splitter.on_text_url_command_text_ctrl)

        return splitter

    @staticmethod
    def build_profile_splitter(*args, **kwargs):
        splitter = Splitter(*args, **kwargs)
        splitter.Bind(wx.EVT_IDLE, splitter.profile_splitter_on_idle)
        splitter.SetMinimumPaneSize(30)

        # Top side:
        splitter.top_panel = wx.Panel(splitter, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
            # Widgets in top side:
        information_static_sizer = wx.StaticBoxSizer(wx.StaticBox(splitter.top_panel, wx.ID_ANY, "Information"), wx.VERTICAL)
        splitter.information_list_ctrl = ObjectListView(information_static_sizer.GetStaticBox(), wx.ID_ANY,
                                                     wx.DefaultPosition, wx.DefaultSize, wx.LC_SINGLE_SEL | wx.LC_SMALL_ICON)
        information_static_sizer.Add(splitter.information_list_ctrl, 1, wx.EXPAND, 5)
            # Set widgets in panel:
        splitter.top_panel.SetSizer(information_static_sizer)
        splitter.top_panel.Layout()
        information_static_sizer.Fit(splitter.top_panel)

        # Bottom side:
        splitter.bottom_panel = wx.Panel(splitter, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
            # Widgets in Bottom side:
        processes_and_services_static_sizer = wx.StaticBoxSizer(wx.StaticBox(splitter.bottom_panel, wx.ID_ANY, "Processes and Services"), wx.VERTICAL)
        splitter.static_text_processes = wx.StaticText(processes_and_services_static_sizer.GetStaticBox(), wx.ID_ANY,
                                                       "Processes:", wx.DefaultPosition, wx.DefaultSize, 0)
        splitter.static_text_processes.Wrap(-1)
        splitter.list_ctrl_processes = ObjectListView(processes_and_services_static_sizer.GetStaticBox(), wx.ID_ANY,
                                                      wx.DefaultPosition, wx.DefaultSize,
                                                      wx.LC_AUTOARRANGE | wx.LC_HRULES | wx.LC_REPORT | wx.LC_SINGLE_SEL | wx.LC_VRULES)
        splitter.static_text_services = wx.StaticText(processes_and_services_static_sizer.GetStaticBox(), wx.ID_ANY,
                                                      "Services:", wx.DefaultPosition, wx.DefaultSize, 0)
        splitter.static_text_services.Wrap(-1)
        splitter.list_ctrl_services = ObjectListView(processes_and_services_static_sizer.GetStaticBox(), wx.ID_ANY,
                                                     wx.DefaultPosition, wx.DefaultSize,
                                                     wx.LC_AUTOARRANGE | wx.LC_HRULES | wx.LC_REPORT | wx.LC_SINGLE_SEL | wx.LC_VRULES)
        processes_and_services_static_sizer.Add(splitter.static_text_processes, 0, wx.RIGHT | wx.LEFT, 5)
        processes_and_services_static_sizer.Add(splitter.list_ctrl_processes, 1, wx.EXPAND | wx.ALL, 5)
        processes_and_services_static_sizer.Add(splitter.static_text_services, 0, wx.RIGHT | wx.LEFT, 5)
        processes_and_services_static_sizer.Add(splitter.list_ctrl_services, 1, wx.ALL | wx.EXPAND, 5)
            # Set widgets in panel:
        splitter.bottom_panel.SetSizer(processes_and_services_static_sizer)
        splitter.bottom_panel.Layout()
        processes_and_services_static_sizer.Fit(splitter.bottom_panel)

        # Set sides in splitter:
        splitter.SplitHorizontally(splitter.top_panel, splitter.bottom_panel, 90)
        return splitter

    def write_introduction_to_shell(self):
        self.shell_rich_text.BeginFontSize(22)
        self.shell_rich_text.BeginBold()
        self.shell_rich_text.WriteText(" Wifi Analyzer ")
        self.shell_rich_text.EndBold()
        self.shell_rich_text.EndFontSize()
        self.shell_rich_text.BeginFontSize(7)
        self.shell_rich_text.BeginBold()
        self.shell_rich_text.WriteText('v1.0')
        self.shell_rich_text.EndBold()
        self.shell_rich_text.EndFontSize()
        self.shell_rich_text.EndTextColour()
        self.shell_rich_text.BeginTextColour('green')
        self.shell_rich_text.WriteText(os.linesep)
        self.shell_rich_text.WriteText(os.linesep)

    # ---------------Events-------------------
    def main_splitter_on_idle(self, event):
        self.SetSashPosition(413)
        self.Unbind(wx.EVT_IDLE)

    def profile_splitter_on_idle(self, event):
        self.SetSashPosition(90)
        self.Unbind(wx.EVT_IDLE)

    def on_text_enter_command_text_ctrl(self, event):
        output = event.GetString() + os.linesep
        write_to_shell(self.shell_rich_text, output)
        self.command_text_ctrl.ChangeValue("")
        event.Skip()

    def on_text_url_command_text_ctrl(self, event):
        mouse_event = event.GetMouseEvent()
        if mouse_event.ButtonDown():
            url = self.command_text_ctrl.GetLineText(0)[event.GetURLStart():event.GetURLEnd()]
            webbrowser.open(url)

    def process_item_right_click(self, event):
        self.computers_list_ctrl.PopupMenu(self.computers_menu)

    def computers_menu_selection(self, option_name, event):
        selection = self.computers_list_ctrl.GetSelectedObject()
        print self.computers_options[option_name](selection)

    def processes_menu_selection(self, option_name, event):
        selection = self.processes_list_ctrl.GetSelectedObject()
        print self.processes_options[option_name](selection)

    def services_menu_selection(self, option_name, event):
        selection = self.services_list_ctrl.GetSelectedObject()
        print self.services_options[option_name](selection)

    # -----------Controller-Functions------------
    def set_computers_columns(self, columns):
        """
            <List> columns = [ (attribute_name, column_name, column_width, column_image), ...]
            column_image = -1   # No Image
        """
        set_columns(self.computers_list_ctrl, columns)

    def set_processes_columns(self, columns):
        """
            <List> columns = [ (attribute_name, column_name, column_width, column_image), ...]
            column_image = -1   # No Image
        """
        set_columns(self.list_ctrl_processes, columns)

    def set_services_columns(self, columns):
        """
            <List> columns = [ (attribute_name, column_name, column_width, column_image), ...]
            column_image = -1   # No Image
        """
        set_columns(self.list_ctrl_services, columns)

    def set_shell_attribute(self, shell):
        self.shell = shell

    def get_shell_stdout(self):
        print_func = lambda st: write_to_shell(self.shell_rich_text, st + os.linesep)
        return print_func

    def set_computers_menu(self, options):
        """
            <Dict> options = {option_name: option_fun, ...}
        """
        self.computers_options = options
        self.computers_menu = wx.Menu()
        for option_name, option_func in options.items():
            menu_item = wx.MenuItem(None, wx.ID_ANY, option_name)
            self.computers_menu.AppendItem(menu_item)
            self.computers_menu.Bind(wx.EVT_MENU, partial(self.computers_menu_selection, option_name), menu_item)

    def set_processes_menu(self, options):
        """
            <Dict> options = {option_name: option_fun, ...}
        """
        self.processes_options = options
        self.processes_menu = wx.Menu()
        for option_name, option_func in options.items():
            menu_item = wx.MenuItem(None, wx.ID_ANY, option_name)
            self.processes_menu.AppendItem(menu_item)
            self.processes_menu.Bind(wx.EVT_MENU, partial(self.processes_menu_selection, option_name), menu_item)

    def set_services_menu(self, options):
        """
            <Dict> options = {option_name: option_fun, ...}
        """
        self.services_options = options
        self.services_menu = wx.Menu()
        for option_name, option_func in options.items():
            menu_item = wx.MenuItem(None, wx.ID_ANY, option_name)
            self.services_menu.AppendItem(menu_item)
            self.services_menu.Bind(wx.EVT_MENU, partial(self.services_menu_selection, option_name), menue_item)

    def repopulate_computers_list(self):
        self.computers_list_ctrl.RepopulateList()

    def repopulate_processes_list(self):
        self.list_ctrl_processes.RepopulateList()

    def repopulate_services_list(self):
        self.list_ctrl_services.RepopulateList()

    def repopulate_computers_list(self):
        self.list_ctrl_computers.Refresh()

    def refresh_computer(self, obj):
        self.computers_list_ctrl.RefreshObject(obj)

    def refresh_process(self, obj):
        self.list_ctrl_processes.RefreshObject(obj)

    def refresh_service(self, obj):
        self.list_ctrl_services.RefreshObject(obj)

    def add_computers(self, objs):
        self.computers_list_ctrl.SetObjects(objs)

    def add_processes(self, objs):
        self.list_ctrl_processes.SetObjects(objs)

    def add_services(self, objs):
        self.list_ctrl_services.SetObjects(objs)
