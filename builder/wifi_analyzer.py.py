# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Jun 17 2015)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc
import wx.aui
import wx.richtext

###########################################################################
## Class MyFrame
###########################################################################

class MyFrame ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( 986,717 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		self.menuBar = wx.MenuBar( 0 )
		self.menu_file = wx.Menu()
		self.menuItem11 = wx.MenuItem( self.menu_file, wx.ID_ANY, u"Exit", wx.EmptyString, wx.ITEM_NORMAL )
		self.menu_file.AppendItem( self.menuItem11 )
		
		self.menuBar.Append( self.menu_file, u"File" ) 
		
		self.menu_database = wx.Menu()
		self.menuItem1 = wx.MenuItem( self.menu_database, wx.ID_ANY, u"Open Kibana Database", wx.EmptyString, wx.ITEM_NORMAL )
		self.menu_database.AppendItem( self.menuItem1 )
		
		self.menuBar.Append( self.menu_database, u"Database" ) 
		
		self.menu_help = wx.Menu()
		self.menuItem = wx.MenuItem( self.menu_help, wx.ID_ANY, u"About...", wx.EmptyString, wx.ITEM_NORMAL )
		self.menu_help.AppendItem( self.menuItem )
		
		self.menuBar.Append( self.menu_help, u"Help" ) 
		
		self.SetMenuBar( self.menuBar )
		
		frame_sizer = wx.BoxSizer( wx.VERTICAL )
		
		self.auiNotebook = wx.aui.AuiNotebook( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.aui.AUI_NB_DEFAULT_STYLE )
		self.auiNotebook_panel1 = wx.Panel( self.auiNotebook, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		auiNotebook_panel1_sizer = wx.BoxSizer( wx.VERTICAL )
		
		self.splitter1 = wx.SplitterWindow( self.auiNotebook_panel1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.SP_3D )
		self.splitter1.Bind( wx.EVT_IDLE, self.splitter1OnIdle )
		self.splitter1.SetMinimumPaneSize( 30 )
		
		self.splitter1_top_panel = wx.Panel( self.splitter1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		splitter1_top_panel_staticSizer = wx.StaticBoxSizer( wx.StaticBox( self.splitter1_top_panel, wx.ID_ANY, u"Computers" ), wx.VERTICAL )
		
		self.top_listCtrl = wx.ListCtrl( splitter1_top_panel_staticSizer.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LC_AUTOARRANGE|wx.LC_HRULES|wx.LC_REPORT|wx.LC_SINGLE_SEL|wx.LC_VRULES )
		splitter1_top_panel_staticSizer.Add( self.top_listCtrl, 1, wx.ALL|wx.EXPAND, 1 )
		
		
		self.splitter1_top_panel.SetSizer( splitter1_top_panel_staticSizer )
		self.splitter1_top_panel.Layout()
		splitter1_top_panel_staticSizer.Fit( self.splitter1_top_panel )
		self.splitter1_bottom_panel = wx.Panel( self.splitter1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		splitter1_bottom_panel_staticSizer = wx.StaticBoxSizer( wx.StaticBox( self.splitter1_bottom_panel, wx.ID_ANY, u"Shell" ), wx.VERTICAL )
		
		self.shell_richText = wx.richtext.RichTextCtrl( splitter1_bottom_panel_staticSizer.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_AUTO_URL|wx.TE_PROCESS_ENTER|wx.TE_PROCESS_TAB|wx.VSCROLL|wx.HSCROLL|wx.NO_BORDER|wx.WANTS_CHARS )
		splitter1_bottom_panel_staticSizer.Add( self.shell_richText, 1, wx.EXPAND|wx.TOP|wx.RIGHT|wx.LEFT, 5 )
		
		self.command_textCtrl = wx.TextCtrl( splitter1_bottom_panel_staticSizer.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		splitter1_bottom_panel_staticSizer.Add( self.command_textCtrl, 0, wx.EXPAND|wx.BOTTOM|wx.RIGHT|wx.LEFT, 5 )
		
		
		self.splitter1_bottom_panel.SetSizer( splitter1_bottom_panel_staticSizer )
		self.splitter1_bottom_panel.Layout()
		splitter1_bottom_panel_staticSizer.Fit( self.splitter1_bottom_panel )
		self.splitter1.SplitHorizontally( self.splitter1_top_panel, self.splitter1_bottom_panel, 413 )
		auiNotebook_panel1_sizer.Add( self.splitter1, 1, wx.EXPAND, 1 )
		
		
		self.auiNotebook_panel1.SetSizer( auiNotebook_panel1_sizer )
		self.auiNotebook_panel1.Layout()
		auiNotebook_panel1_sizer.Fit( self.auiNotebook_panel1 )
		self.auiNotebook.AddPage( self.auiNotebook_panel1, u"Main", True, wx.NullBitmap )
		self.auiNotebook_panel2 = wx.Panel( self.auiNotebook, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		auiNotebook_panel1_sizer = wx.BoxSizer( wx.VERTICAL )
		
		self.splitter2 = wx.SplitterWindow( self.auiNotebook_panel2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.SP_3D )
		self.splitter2.Bind( wx.EVT_IDLE, self.splitter2OnIdle )
		self.splitter2.SetMinimumPaneSize( 30 )
		
		self.splitter2_top_panel = wx.Panel( self.splitter2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		splitter2_top_panel_staticSizer = wx.StaticBoxSizer( wx.StaticBox( self.splitter2_top_panel, wx.ID_ANY, u"Information" ), wx.VERTICAL )
		
		self.listCtrl_information = wx.ListCtrl( splitter2_top_panel_staticSizer.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LC_SINGLE_SEL|wx.LC_SMALL_ICON )
		splitter2_top_panel_staticSizer.Add( self.listCtrl_information, 1, wx.EXPAND, 5 )
		
		
		self.splitter2_top_panel.SetSizer( splitter2_top_panel_staticSizer )
		self.splitter2_top_panel.Layout()
		splitter2_top_panel_staticSizer.Fit( self.splitter2_top_panel )
		self.splitter2_bottom_panel = wx.Panel( self.splitter2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		splitter2_bottom_panel_staticSizer = wx.StaticBoxSizer( wx.StaticBox( self.splitter2_bottom_panel, wx.ID_ANY, u"Processes and Services" ), wx.VERTICAL )
		
		self.staticText_processes = wx.StaticText( splitter2_bottom_panel_staticSizer.GetStaticBox(), wx.ID_ANY, u"Processes:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticText_processes.Wrap( -1 )
		splitter2_bottom_panel_staticSizer.Add( self.staticText_processes, 0, wx.RIGHT|wx.LEFT, 5 )
		
		self.listCtrl_processes = wx.ListCtrl( splitter2_bottom_panel_staticSizer.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LC_AUTOARRANGE|wx.LC_HRULES|wx.LC_REPORT|wx.LC_SINGLE_SEL|wx.LC_VRULES )
		splitter2_bottom_panel_staticSizer.Add( self.listCtrl_processes, 1, wx.EXPAND|wx.ALL, 5 )
		
		self.staticText_services = wx.StaticText( splitter2_bottom_panel_staticSizer.GetStaticBox(), wx.ID_ANY, u"Services:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.staticText_services.Wrap( -1 )
		splitter2_bottom_panel_staticSizer.Add( self.staticText_services, 0, wx.RIGHT|wx.LEFT, 5 )
		
		self.listCtrl_services = wx.ListCtrl( splitter2_bottom_panel_staticSizer.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LC_AUTOARRANGE|wx.LC_HRULES|wx.LC_REPORT|wx.LC_SINGLE_SEL|wx.LC_VRULES )
		splitter2_bottom_panel_staticSizer.Add( self.listCtrl_services, 1, wx.ALL|wx.EXPAND, 5 )
		
		
		self.splitter2_bottom_panel.SetSizer( splitter2_bottom_panel_staticSizer )
		self.splitter2_bottom_panel.Layout()
		splitter2_bottom_panel_staticSizer.Fit( self.splitter2_bottom_panel )
		self.splitter2.SplitHorizontally( self.splitter2_top_panel, self.splitter2_bottom_panel, 90 )
		auiNotebook_panel1_sizer.Add( self.splitter2, 1, wx.EXPAND, 1 )
		
		
		self.auiNotebook_panel2.SetSizer( auiNotebook_panel1_sizer )
		self.auiNotebook_panel2.Layout()
		auiNotebook_panel1_sizer.Fit( self.auiNotebook_panel2 )
		self.auiNotebook.AddPage( self.auiNotebook_panel2, u"Profile", False, wx.NullBitmap )
		
		frame_sizer.Add( self.auiNotebook, 1, wx.ALL|wx.EXPAND, 1 )
		
		
		self.SetSizer( frame_sizer )
		self.Layout()
		self.statusBar = self.CreateStatusBar( 1, wx.ST_SIZEGRIP, wx.ID_ANY )
		
		self.Centre( wx.BOTH )
	
	def __del__( self ):
		pass
	
	def splitter1OnIdle( self, event ):
		self.splitter1.SetSashPosition( 413 )
		self.splitter1.Unbind( wx.EVT_IDLE )
	
	def splitter2OnIdle( self, event ):
		self.splitter2.SetSashPosition( 90 )
		self.splitter2.Unbind( wx.EVT_IDLE )
	

